#!/usr/bin/guile -s
!#

(use-modules (srfi srfi-1)
	     (ice-9 popen)
	     (ice-9 regex)
	     (ice-9 textual-ports))

(define carrot-path "")

(if (getenv "RABBIT_DIR")
    (set! carrot-path (string-append (getenv "RABBIT_DIR") "/carrots"))
    (set! carrot-path (string-append (getenv "HOME") "/.config/rabbit/carrots")))

(load (string-append carrot-path "/rabbit.scm"))
(rabbit-refresh carrot-path)

(define get-window-details
  (lambda ()
    (let* ([port (open-input-pipe "xwininfo")]
	   [output (get-string-all port)])
      (close-pipe port)

      ;; This is a pretty verbose line but it's essentially just getting the
      ;; fourth word of the sixth line.
      (fourth (string-split (sixth (string-split output #\newline)) #\space)))))

(define rabbit-window
  (lambda ()
    (display "Please select a window for Rabbit to run\n")
    (get-window-details)))

(define xev-pipe
  (lambda (win)
    (open-input-pipe (string-append "xev -event keyboard -id " win))))

(define get-keystroke
  (lambda (pipe)
    (let ([line (get-line pipe)])
      (if (string-match "KeyRelease" line) (get-keypress pipe)
	  (if (string-match "state" line)
	      (cond [(string-match "space" line) #\space]
		    [(string-match "Return" line) #\return]
		    [(string-match "BackSpace" line) #\backspace]
		    [else (get-keystroke pipe)])
	      (if (and (string-match "XLookupString" line) (string-match "1" line))
		  (cadr (reverse (string->list line)))
		  (get-keystroke pipe)))))))

(define handle-char
  (let ([carrot #f]
	[command '()])
    (lambda (char pipe)
      (if (char=? char #\^)
	  (if carrot (begin (unless (null? command)
			      (carrot-expansion (list->string
						 (reverse command))))
			    (set! carrot #f)
			    (set! command '()))
	      (set! carrot #t))
	  (when carrot
	    (cond [(char=? char #\backspace) (if (null? command)
						 (set! carrot #f)
						 (set! command (cdr command)))]
		  [(char=? char #\return) (begin (set! carrot #f) (set! command '()))]
		  [else (set! command (cons char command))])))
      (get-keypress pipe))))

(define write-backspace
  (lambda (count)
    (system "xdotool key --delay 0 BackSpace")
    (when (> count 1) (write-backspace (- count 1)))))

(define write-string
  (lambda (str)
    (system (string-append "xdotool type --delay 0 " str))))

(define expand
  (lambda (expansion len)
    (when (> (string-length expansion) 2)
      (write-backspace len)
      (write-string expansion))))

(define carrot-expansion
  (lambda (carrot)
    (let* ([parts (string-split carrot #\space)]
	   [len (+ (string-length carrot) 2)]
	   [func (symbol-append 'r- (string->symbol (car parts)))])
      (when (defined? func)
	(expand (string-append "\""
			       (apply (eval func (interaction-environment))
				      (if (> (length parts) 1) (list (cdr parts)) '()))
			       "\"")
		len)))))

(define get-keypress
  (lambda (pipe)
    (let ([line (get-line pipe)])
      (if (string-match "KeyPress" line)
	  (handle-char (get-keystroke pipe) pipe)
	  (get-keypress pipe)))))

(get-keypress (xev-pipe (rabbit-window)))

(newline)
