;; Syntax Extensions
(define-syntax route
  (syntax-rules ()
    ([_ input (test output) ...]
     (cond [(if (list? test)
		(member (car input) test)
		(string=? (car input) test)) (output (cdr input))] ...
	   [else ""]))))

;; Rabbit Functions
(define r-rabbit
  (lambda (parts)
    (route parts
	   [(list "r" "refresh") (rabbit-refresh carrot-path)])))

(define rabbit-refresh
  (lambda (path)
    (lambda (discard)
      (let ([opened-dir (opendir path)])
	(load-carrots opened-dir)
	(closedir opened-dir)
	"complete"))))

(define load-carrots
  (lambda (dir)
    (let ([file (readdir dir)])
      (unless (eof-object? file)
	(when (string-match ".scm" file)
	  (load file))
	(load-carrots dir)))))
