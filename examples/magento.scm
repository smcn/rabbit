(define r-magento
  (lambda (parts)
    (let ([args (cdr parts)])
     (route parts
	   [(list "rebuild" "r") rebuild]
	   [(list "cache" "c" "index" "i") cache]
	   ["admin" admin]))))

(define rebuild
  (lambda (args)
    (let ([bin-magento (string-append "php-" (car args) " bin/magento ")])
      (string-append bin-magento "maintenance:enable && "
		     bin-magento "setup:upgrade && "
		     bin-magento "setup:di:compile && "
		     bin-magento "setup:static-content:deploy && "
		     bin-magento "cache:flush && "
		     bin-magento "maintenance:disable"))))

(define cache
  (lambda (args)
    (let ([bin-magento (string-append "php-" (car args) " bin/magento ")])
      (string-append bin-magento "index:reindex && "
		     bin-magento "cache:flush && "
		     bin-magento "cache:clean"))))

(define admin
  (lambda (args)
    (let ([bin-magento (string-append "php-" (car args) " bin/magento ")])
      (string-append (create-admin-user bin-magento "john") " && "
		     (create-admin-user bin-magento "paul") " && "
		     (create-admin-user bin-magento "joseph")))))

(define create-admin-user
  (lambda (bin-magento user)
    (string-append bin-magento
		   "admin:user:create --admin-user='" user "' " 
		   "--admin-password='password' "
		   "--admin-email='" user "@example.com' "
		   "--admin-firstname='Firstname' "
		   "--admin-lastname='LastName'")))
