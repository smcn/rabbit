# Rabbit
Rabbit is a(n almost) system-wide text expansion program.

## Dependencies
1) Guile Scheme v2.2
2) xdotool
3) xev

## Setup
Clone this repository to somewhere on your local system, and then place `rabbit.scm` in your `$PATH` (I personally symlink it to `~/bin` but you do you). After you have done this, you need to setup the `$RABBIT_DIR`. This can be done in one of two ways:

1) Create `~/.config/rabbit/carrots`. This is the default functionality. 
2) Set `$RABBIT_DIR` in your shell's RC file. This can be anything you want it to be but you just need to make sure that there is a folder called `carrots` within it.

After you have set this, please symlink `rabbit_functions.scm` to `$RABBIT_DIR/carrots`.

## Usage
In your terminal, type `rabbit.scm` and then click the window that you wish to track. **Be careful as not all windows work with Rabbit.** This is because `xev` is not able to track the keypresses on certain windows. Your mileage will vary. Assuming that you're connected to a window that does function correctly, you can now start expanding some text. Before we get there though, let's talk about function definitions.

## Defining Carrots
All files that end in ".scm" within the `$RABBIT_DIR/carrots` directory will be evaluated when you start Rabbit, or refresh Rabbit. Files can be called whatever you wish, there is no limit (outside of traditional unix limits). I typically group my function based on task and name the file so.

If you wish to make a function accessible for expansion then it must be prefixed by "r-", for example: "r-vim". Functions must also take a single argument which will be a list of the words after the function call. For instance, if we were to type \^rabbit refresh\^, Rabbit will run the `r-rabbit` command with the argument of `(list refresh).` Let's look at how `r-rabbit` is defined:

    (define r-rabbit
      (lambda (parts)
        (route (car parts)
    	   [(list "r" "refresh") (rabbit-refresh carrot-path)])))

`route` is a macro which takes the test string `(car parts)` (which is "refresh") and routes it until it finds a match, which will either be the string itself, or a list that contains it. Here we can see that \^rabbit refresh\^ will work, as will \^rabbit r\^, which will then evaluate `(rabbit-refresh carrot-path)` (the details of which you can find in `rabbit_functions.scm`). It is recommended that, when defining your own functions, you ensure that they return strings as, well, this is a text expansion program.

Please see the examples folder for more functions.

## FAQ
### Why Rabbit?
\^ is called a Caret. This program eats them. Rabbits eat carrots. Might be the greatest pun I've ever made. 50% of the motivation to finish this program was to use the pun.
### Rabbit is really slow!
I'll get back to you on this one.
